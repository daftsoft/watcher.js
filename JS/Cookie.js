/*
 * 
 * (C) DaftSoft, 2014-2020
 * Daft-Soft.blogspot.com
 * gitlab.com/users/DaftSoft
 * DaftSoft@yandex.ru
 * 
 */

function SetCookie (_Name, _Value, _ExDays)
{
  var ExDate = new Date();
  ExDate.setDate (ExDate.getDate() + _ExDays);
  var lValue = escape(_Value) + ((_ExDays == null) ? "" : "; expires=" + ExDate.toUTCString());
  document.cookie = _Name + "=" + lValue;
}

function GetCookie (_Name)
{
  var i, x, y, ARRcookies = document.cookie.split(";");
  for (i = 0; i < ARRcookies.length; i++)
  {
    x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
    x = x.replace(/^\s+|\s+$/g, "");
    if (x == _Name)
      return unescape (y);
  }
  return "";
}

function RemoveCookie (_Name)
{
  SetCookie (_Name, "", 0);
}
