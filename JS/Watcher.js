/*
 * 
 * (C) DaftSoft, 04.2014-04.2020
 * Daft-Soft.blogspot.com
 * gitlab.com/users/DaftSoft
 * DaftSoft@yandex.ru
 * 
 */
var WatchedFile = "";
var WatcherVersion = "0.1";
var CurrentLine = 1;
var LinesLeftToFetch = 0;
var InitialFetchPartSize = 30;
var FetchPartSize = InitialFetchPartSize;
var InitialFileSize = 0;
var LastFetch = false;
var AccessError = false;
var FileWatchTimer = null;
var FileWatchInterval = 3000;
var CurrentLineCounter = 1;
var WatchedLinesCounter = 0;

// Requests:
var WatchRequest;
var ClearLogRequest;
var FetchPartRequest;
var GetFileSizeRequest;
var DownloadLogRequest;

var RequestCommand = "";
var CGIPathPrefix = "./CGI/";

function GetXMLHTTP()
{
  var XMLHTTP;
  try
  {
    XMLHTTP = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (_E)
  {
    try
    {
      XMLHTTP = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (_E)
    {
      XMLHTTP = false;
    }
  }
  if (!XMLHTTP && typeof XMLHttpRequest!='undefined')
  {
    XMLHTTP = new XMLHttpRequest();
  }
  //XMLHTTP.setRequestHeader('Pragma', 'Cache-Control: no-cache');
  return XMLHTTP;
}

function RequestDone (_Request)
{
  if ((_Request.readyState === XMLHttpRequest.DONE) &&
      (_Request.status === 0 || (_Request.status >= 200 && _Request.status < 400)))
    return true;
  else
    return false;
}

// LEI stands for Leave Empty (Strings) Intact:
function SplitParamsLEI (_String)
{
  lParamsArray = _String.split ('\n');
  if (lParamsArray.length <= 0)
    return;
  return lParamsArray;
}

function GetURLParam (_Name)
{
  _Name = _Name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var lRegExS = "[\\?&]" + _Name + "=([^&#]*)";
  var lRegEx = new RegExp( lRegExS );
  var lResults = lRegEx.exec( window.location.href );
  if( lResults === null )
    return "";
  else
    return lResults[1];
}

function InitWatcher ()
{
  // Выставляем значение переключателя "AutoScroll":
  if (GetCookie ("WatcherAutoScrollSwitch") == "true")
    document.getElementById("LogAutoScrollCheckBox").checked = true;
  else
    document.getElementById("LogAutoScrollCheckBox").checked = false;    
  // Создаём объекты запросов по HTTP:
  window.WatchRequest = GetXMLHTTP();
  window.ClearLogRequest = GetXMLHTTP();
  window.FetchPartRequest = GetXMLHTTP();
  window.GetFileSizeRequest = GetXMLHTTP();
  window.DownloadLogRequest = GetXMLHTTP();
  if (!window.WatchRequest || !window.FetchPartRequest || !window.GetFileSize)
    alert ("HTTP Requests are not supported by this browser.");
  // Вычтавляем всем им обработчики ответов:
  window.WatchRequest.onreadystatechange = WatchResponse;
  window.ClearLogRequest.onreadystatechange = ClearLogResponse;
  window.FetchPartRequest.onreadystatechange = FetchPartResponse;
  window.GetFileSizeRequest.onreadystatechange = GetFileSizeResponse;
  window.DownloadLogRequest.onreadystatechange = DownloadLogResponse;
   // Имя наблюдаемого файла получаем из параметра адресной строки:
  window.WatchedFile = GetURLParam ("File");
  document.title = "Watcher v." + 0.1 + " (" + decodeURIComponent(window.WatchedFile).replace(/^.*[\\\/]/, '') + ")";
  GetFileSize ();
}

function WatchFunc ()
{
  if (window.FileWatchTimer)
    clearInterval(window.FileWatchTimer);
  window.RequestCommand = CGIPathPrefix + "Watcher.py?Oper=ReceivePart&From=" + window.CurrentLine + "&File=" + window.WatchedFile + "&Size=" + window.FetchPartSize;
  window.WatchRequest.open('GET', window.RequestCommand, true);
  window.WatchRequest.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  window.WatchRequest.send(null);  
}

function GetFileSize ()
{
  window.RequestCommand = CGIPathPrefix + "Watcher.py?Oper=GetSize&File=" + window.WatchedFile;
  window.GetFileSizeRequest.open('GET', window.RequestCommand, true);
  window.GetFileSizeRequest.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  window.GetFileSizeRequest.send(null);
}

function FetchPart ()
{
  if (window.LinesLeftToFetch < window.FetchPartSize)
  {
    window.FetchPartSize = window.LinesLeftToFetch;
    window.LastFetch = true;
  }
  window.RequestCommand = CGIPathPrefix + "Watcher.py?Oper=FetchPart&File=" + window.WatchedFile + "&From=" + parseInt(window.CurrentLine) + "&To=" + parseInt(window.CurrentLine + window.FetchPartSize);
  window.FetchPartRequest.open('GET', window.RequestCommand, true);
  window.FetchPartRequest.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  window.FetchPartRequest.send(null);
}

function ParseLines (_Lines, _Type)
{
  var lNewDIV = null;
  for (lI = 0; lI < _Lines.length; lI++)
  {
    lNewDIV = document.createElement("div");
    lNewDIV.innerHTML = "";
    lNewDIV.innerHTML = lNewDIV.innerHTML + "<span STYLE='color: orange; width: 50px; height: auto; display: inline-block'>" + parseInt (window.CurrentLineCounter) + ":</span>";
    window.CurrentLineCounter = window.CurrentLineCounter + 1;  
    if (_Type === 0)
      document.getElementById("FetchStatus").innerHTML = "Fetching " + window.LinesLeftToFetch + " lines of " + decodeURIComponent(window.WatchedFile).replace(/^.*[\\\/]/, '') + "...";
    else
    {
      window.WatchedLinesCounter = window.WatchedLinesCounter + 1;
      // Изменяем статус:
      document.getElementById("WatchStatus").innerHTML = "<span class='Orange'>Watching...</span> Received " + window.WatchedLinesCounter + " new line(s) of " + decodeURIComponent(window.WatchedFile).replace(/^.*[\\\/]/, '') + "...";
    }
    lNewDIV.innerHTML = lNewDIV.innerHTML + _Lines[lI].replace(/ /g, '\u00a0');
    document.getElementById("Watcher").appendChild (lNewDIV);
  }
  if ((document.getElementById("LogAutoScrollCheckBox").checked) && (_Lines.length) > 0)
    document.getElementById("Watcher").scrollTop = document.getElementById("Watcher").scrollHeight;
}

function ClearLog ()
{
  if (!confirm("Are you sure want to clear log?"))
    return;
  window.RequestCommand = CGIPathPrefix + "Watcher.py?Oper=ClearLog&File=" + window.WatchedFile;
  window.ClearLogRequest.open('GET', window.RequestCommand, true);
  window.ClearLogRequest.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  window.ClearLogRequest.send(null);
}

function EmptyWatcher ()
{
  while (document.getElementById("Watcher").hasChildNodes())
    document.getElementById("Watcher").removeChild(document.getElementById("Watcher").lastChild);
}

function ResetLog ()
{
  window.CurrentLine = 1;
  window.LinesLeftToFetch = 0;
  window.InitialFetchPartSize = 30;
  window.FetchPartSize = InitialFetchPartSize;
  window.InitialFileSize = 0;
  window.LastFetch = false;
  window.FileWatchTimer = null;
  window.CurrentLineCounter = 1;
  window.WatchedLinesCounter = 0;
  EmptyWatcher ();
  GetFileSize ();
}

function DownloadLog ()
{
  window.RequestCommand = CGIPathPrefix + "Watcher.py?Oper=DownloadLog&File=" + window.WatchedFile;
  window.DownloadLogRequest.open('GET', window.RequestCommand, true);
  window.DownloadLogRequest.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  window.DownloadLogRequest.send(null);
}

function WatchResponse ()
{
  if (RequestDone (window.WatchRequest))
  {
    var lParams = SplitParamsLEI (window.WatchRequest.responseText);
    if (lParams[lParams.length - 1].length === 0)
      lParams.splice (-1, 1);
    window.CurrentLine = window.CurrentLine + lParams.length;
    ParseLines (lParams, 1);
    // Выставляем интервал заново:
    FileWatchTimer = setInterval ("WatchFunc ()", FileWatchInterval);
  }
}

function FetchPartResponse ()
{
  if (RequestDone (window.FetchPartRequest))
  {
    // Получили кусок фаила:
    var lParams = SplitParamsLEI  (window.FetchPartRequest.responseText);
    // Убираем пустую строку спереди:
    if (lParams[lParams.length - 1].length === 0)
    lParams.splice (-1, 1);
    // Передвигаем указатель текущей строки на то количество, которое получено:
    window.CurrentLine = window.CurrentLine + window.FetchPartSize + 1;
    // Уменьшаем количество оставшихся к вытягиванию строк на количество, которое получено:
    window.LinesLeftToFetch = window.LinesLeftToFetch - lParams.length;
    ParseLines (lParams, 0);

    if ((window.LinesLeftToFetch > 0) && (window.InitialFileSize + 1 >= window.CurrentLine))
      FetchPart ();
    else
    {
      document.getElementById("FetchStatus").innerHTML = "Fetched " + parseInt(window.CurrentLineCounter - 1)  + " Lines of " + decodeURIComponent(window.WatchedFile).replace(/^.*[\\\/]/, '');
      window.FetchPartSize = window.InitialFetchPartSize;
      window.CurrentLine = window.InitialFileSize + 1;
      // Включаем наблюдающий таймер:
      // Сбрасываем таймер, если он взведён:
      if (FileWatchTimer)
        clearInterval(FileWatchTimer);
      // Выставляем таймер:
      FileWatchTimer = setInterval ("WatchFunc ()", FileWatchInterval);
      document.getElementById("WatchStatus").innerHTML = "<span class='Orange'>Watching...</span>";
    }
  }
}

function GetFileSizeResponse ()
{
  if (RequestDone (window.GetFileSizeRequest))
  {
    window.LogInit = true;
    //lParams = SplitParamsLEI  (lXMLHTTP.responseText);
    window.InitialFileSize = parseInt (window.GetFileSizeRequest.responseText);
    if (Number.isNaN(window.InitialFileSize))
    {
      AccessError = true;
      document.getElementById("FetchStatus").innerHTML = "Error! File does not exist or Access Denied (" + decodeURIComponent(window.WatchedFile) + ")." ;
      return;
    }
    AccessError = false;
    window.LinesLeftToFetch = window.InitialFileSize + 1;
    document.getElementById("FetchStatus").innerHTML = "Fetching " + window.LinesLeftToFetch + " Lines...";
    FetchPart ();
  }
}

function DownloadLogResponse ()
{
  if (RequestDone (window.DownloadLogRequest))
    window.open (window.WatchedFile, "Download Log");
}

function ClearLogResponse ()
{
  if (RequestDone (window.ClearLogRequest))
    ResetLog ();
}
