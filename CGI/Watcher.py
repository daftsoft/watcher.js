#!/usr/bin/python2
# -*- coding: utf-8 -*-
import cgi
import sys
import subprocess
  
sys.stdout.write("Content-Type: text/html\n\n")

def Capture (_Command):
  return subprocess.check_output(_Command, shell=True)

def GetQSValue(_Value):
  # Получаем Query String:
  QueryString = cgi.FieldStorage()
  if _Value not in QueryString:
    return ""
  else:
    return QueryString[_Value].value

def main ():
  # Префикс пути к "наблюдаемым" файлам:
  PathPrefix=""
  # Получаем имя файла:
  File = PathPrefix + GetQSValue("File")
  # Получаем заданную операцию:
  Oper=GetQSValue("Oper")
  
  # Если операция не указана, выходим:
  if Oper=="":
    sys.exit()
  
  # Обрабатываем запрос на получение размера файла (в строках):
  if Oper=="GetSize":
    Command = "wc -l < " +  File + " | xargs echo -n"
    sys.stdout.write (Capture (Command))

  # Обрабатываем запрос на получение части файла по началу и концу блока (в строках):
  if Oper=="FetchPart":
    From = GetQSValue ("From")
    To = GetQSValue ("To")
    Command = "sed -n '" + From +  "," + To + "p' " +  File
    sys.stdout.write (Capture (Command))
  
  # Обрабатываем запрос на получение части файла по началу и размеру блока (в строках):
  if Oper=="ReceivePart":
    CurrentLine = int(GetQSValue ("From"))
    Size = int(GetQSValue ("Size"))
    Command = "cat " + File + " | wc -l"
    FileSize = int(Capture (Command))
    To = (CurrentLine + Size)
    if FileSize >= CurrentLine:
      Command = "sed -n '" + str(CurrentLine) +  "," + str(To) + "p;' " +  File
      sys.stdout.write (Capture (Command))
    
  # Обрабатываем запрос на очистку файла:
  if Oper=="ClearLog":
    subprocess.call ("echo -n \"\" > " + File, shell=True)
    
if (__name__=='__main__'):
  main()
